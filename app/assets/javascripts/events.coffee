# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on 'ready page:change', ->
    $('input.selectize[type="text"][name="event[user_input_teller_ids]"]').selectize({
        plugins: ['remove_button'],
        selectOnTab: true,
        delimiter: ',',
        persist: false,
        create: false,
        valueField: 'id',
        searchField: 'name',
        labelField: 'name',
        render: {
            option: (item, escape) ->
                return """
                        <div>
                        <span class="title">#{escape(item.name)}</span>
                        </div>
                       """
        },
        load: (query, callback) ->
             return callback() if (!query.length)
             $.ajax({
                 url: '/manage/tellers.json',
                 type: 'GET',
                 error: -> callback() ,
                 success: (res) ->
                    callback(res)
             })
            
    })

    $('select.selectize[name="event[location_id]"]').selectize({
        selectOnTab: false,
        delimiter: ',',
        persist: false,
        create: false,
        valueField: 'id',
        labelField: 'name',
        searchField: 'name',
        allowEmptyOption: false,
        render: {
            option: (item, escape) ->
                return """
                        <div>
                            <span class="title">#{escape(item.name)}</span>
                        </div>
                       """
        },
        load: (query, callback) ->
            return callback() if (!query.length)
            $.ajax({
                url: '/manage/locations.json',
                type: 'GET',
                error: -> callback() ,
                success: (res) ->
                    callback(res.slice(0,10))
            })
    })

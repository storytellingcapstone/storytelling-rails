var activateDataTables = function() {
  // Initialize datatables on the page.
  // Supports properties for hiding columns and disabling sorting.
  //
  // Prevent sorting the last two columns.
  // data-datatable-not-orderable="-1,-2"
  //
  // Prevent seeing the second column.
  // data-datatable-not-visible="1"
  $("table.dataTable").each(function() {
    var order = $(this).attr("data-datatable-order");
    var order_col = 0;
    var order_direction = "asc";
    var not_orderable = $(this).attr("data-datatable-not-orderable");
    var not_visible = $(this).attr("data-datatable-not-visible");

    if(order) {
      order = order.split(",");
      order_col = parseInt(order[0]);
      order_direction = order[1] || order_direction;
    }

    if(not_orderable) {
      not_orderable = not_orderable.split(",");

      for(i=0; i < not_orderable.length; i++) {
        not_orderable[i] = parseInt(not_orderable[i]);
      }
    } else {
      not_orderable = [];
    }

    if(not_visible) {
      not_visible = not_visible.split(",");

      for(i=0; i < not_visible.length; i++) {
        not_visible[i] = parseInt(not_visible[i]);
      }
    } else {
      not_visible = [];
    }

    $(this).dataTable({
      "order": [ order_col, order_direction ],
      "columnDefs": [
      { "orderable": false, "targets": not_orderable },
      { "visible": false, "aTargets": not_visible }
      ]
    });
  });
};

$(document).on("ready page:load", function() {
  // Start DataTables
  activateDataTables();

  // Start datetimepickers
  $("input.dtp").each(function() {
    // DateTime
    $(this).datetimepicker();
  });

  $("input.dtp-date-only").each(function() {
    $(this).datetimepicker({
      // Date only
      format: 'MM/DD/YYYY'
    });
  });
});

$(document).on("page:change", function() {
});

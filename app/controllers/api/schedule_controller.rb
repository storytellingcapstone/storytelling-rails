class Api::ScheduleController < ApplicationController
  def sync
    # Change this value to change the length of time that results are cached.
    @cache_time = 10.minutes

    @schedule = Rails.cache.fetch("api-schedule", expires_in: @cache_time, race_condition_ttl: 5.seconds) do
      Schedule.order("end_at DESC").first
    end

    if @schedule.blank?# && @schedule.updated_at > sync_date
      # We should do something more useful if no schedule is returned.
      render nothing: true
      return
    end

    @vendors = Vendor.active

    respond_to do |format|
      format.html { render html: 'Invalid use of API' }
      format.json { render }
    end
  end
end

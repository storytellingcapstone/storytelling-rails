module ApplicationHelper
  # Nav links that automatically have active class.
  # Thanks to: http://stackoverflow.com/a/20369069
  # Updated to mark links active if displaying an action besides index
  # e.g. /manage/admins/show/1 # The /manage/admins link would be active.
  def nav_link(*args, &block)
    is_active = current_page?(args[0]) || current_page?(args[1]) || request.fullpath.starts_with?(args[1])
    class_name = is_active ? 'active' : nil
    content_tag(:li, class: class_name) do
      link_to(*args, &block)
    end
  end
end

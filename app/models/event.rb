class Event < ActiveRecord::Base
  # This is really not ideal.
  # It causes lots of problems with queries and such.
  # If we could store UTC and only display EST, that would probably be a better solution in hindsight.
  # We are ignoring timezone changes because EST changes to EDT and breaks the schedule.
  self.skip_time_zone_conversion_for_attributes = [ :start, :end_at ]

  # In hindsight, this is a poorly named table.
  # It's simply the join table for events and tellers.
  has_many :event_teller_schedules
  has_many :tellers, through: :event_teller_schedules

  belongs_to :location

  validates :title, presence: true
  validates :start, presence: true
  validates :end_at, timeliness: { after: :start, type: :datetime, after_message: "date must be after start date." }

  attr_accessor :user_input_teller_ids

  before_validation :process_user_input_teller_ids

  # Override setter to parse US date.
  def start=(v)
    begin
      write_attribute(:start, DateTime.strptime(v, "%m/%d/%Y %I:%M %p"))
    rescue ArgumentError, NoMethodError
      errors.add(:start, "is not a valid date")
    end
  end

  # Override setter to parse US date.
  def end_at=(v)
    begin
      write_attribute(:end_at, DateTime.strptime(v, "%m/%d/%Y %I:%M %p"))
    rescue ArgumentError, NoMethodError
      errors.add(:end_at, "is not a valid date")
    end
  end


  # Query schedules containing with this event.
  # Only show schedules that end in the future.
  def schedules
    # This where clause would probably be clearer as "(start <= ? AND end_at >= ?) AND end_at >=?", but
    # if it ain't broke, don't fix it.
    # However, keep that in mind if you need to change this.
    Schedule.where("start <= ? AND end_at >= ? AND end_at >= ?", self.start.to_date, self.end_at.to_date, Date.today).order(:start)
  end

  private

  # Process comma-separated teller ID's from the event form.
  # They are used by the selectize.js library to provide
  # a nice UI tool for selecting tellers. Having it pass
  # id's on submit seemed to be the simplest way to make this happen.
  def process_user_input_teller_ids
    unless user_input_teller_ids.blank?
      ids = user_input_teller_ids.split ","
      begin
        original_teller_ids = self.teller_ids

        # Clearing the tellers first preserves the new order.
        # Rails is smart enough not to touch existing records, so
        # this prevents it from placing future edits at the end of
        # the list.
        self.tellers.clear
        self.teller_ids = ids
      rescue ActiveRecord::RecordNotFound
        self.errors.add(:tellers, " - invalid teller ID given.")

        self.teller_ids = original_teller_ids
      end
    end
  end
end

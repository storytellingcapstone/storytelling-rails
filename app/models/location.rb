class Location < ActiveRecord::Base
    has_many :events

    validates :name, presence: true, length: {:maximum => 100}
end

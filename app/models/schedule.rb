class Schedule < ActiveRecord::Base
  mount_uploader :map, MapUploader
  
  attr_accessor :user_input_start, :user_input_end
  before_validation :process_user_input_dates

  validates :title, :start, :end_at, presence: true
  validates :start, timeliness: { type: :date }
  validates :end_at, timeliness: { after: :start, type: :date, after_message: "must be after start date." }

  # Note: The tellers method currently relies on this one to correctly
  # retrieve the tellers for the schedule. If this method is changed
  # in some strange way, the tellers method may need to be updated.
  # This also applies to the locations method.
  def events
    # We chose to connect Events with schedules dynamically in an effort to save the user effort.
    # We thought they may want to reuse events and/or enter them before creating the schedule they belong
    # to. The idea was that using the dates and times that they needed to enter anyway would save them from
    # having to go back and forth to think about which schedule an event belongs in and to move them around.
    # It ultimately works, but it may have been better to stick with a has_many relationship and either
    # make the user explicitly choose a schedule, or to come up with a different UI for managing them.
    #
    # There has to be a better way than this ugly hack to compare the dates with
    # datetimes correctly.
    Event.where("start >= ? AND end_at <= ?", start.beginning_of_day.to_datetime.change(offset: "+0000"), self[:end_at].end_of_day.to_datetime.change(offset: "+0000")).includes(:tellers)
  end

  def tellers
    # The call below works regardless of how the events method is defined.
    # Teller.joins(:events).where("start >= ? AND end <= ?", start, self[:end])

    # Using the events method, we reduce duplication, but may also introduce a problem
    # if that method requires something unique.
    # Theoretically, the tellers should always be pulled from the events which should
    # be defined correctly in that method.
    Teller.joins(:events).merge(events).unscope(:includes).distinct
  end

  def locations
    # This depends on the events method being defined sanely.
    Location.joins(:events).merge(events).unscope(:includes).distinct
  end

  private

  # Process dates entered by the user on the Schedule form.
  # This is used to allow the user to enter US-formatted dates
  # (m/d/y) instead of Ruby's default of the more common international
  # format (d/m/y).
  # There may be a better way to do this, but I couldn't find one at
  # the time.
  def process_user_input_dates
    [ [user_input_start, :start], [user_input_end, :end_at] ].each do |input, attribute|
      unless input.blank?
        self[attribute] = Date.strptime(input, "%m/%d/%Y")
      end
    end
  end
end

class Teller < ActiveRecord::Base
  mount_uploader :avatar, AvatarUploader

  has_many :event_teller_schedules
  has_many :events, through: :event_teller_schedules

  validates :first_name, :last_name, presence: true, length: {:maximum => 100}
  validates :website, url: { allow_blank: true, message: "is not a valid URL. Do not forget to include http://..."}

  # Normalize the phone number
  phony_normalize :phone, default_country_code: 'US'
  # Validate the normalized number
  validates :phone, phony_plausible: { message: 'is an invalid phone number. Do not forget to include the area code.'}

  def name
    "#{first_name} #{last_name}"
  end

  def upcoming_events
    # Unfortunately, .change(...) is required because we are ignoring timezones for
    # the start field. DateTime is automatically converted to UTC which throws off
    # our comparison unless we use .change() to convince it that it's already UTC.
    # Yes, this is ugly, and can be fixed if a suitable solution for storing UTC,
    # but still ignoring timezone changes is found.
    events.where("start >= ?", DateTime.now.change(offset: "+0000")).order(:start)
  end
end

class Vendor < ActiveRecord::Base
	STATUS = ["active", "inactive"]
  validates :name,:location, presence: true, length: {:maximum => 100}


  validates :status, :inclusion => { :in => STATUS,
    :message => "%{value} is not a valid status" }

  scope :active, lambda{where(:status=>'active')}
end

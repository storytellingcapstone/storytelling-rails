json.cache! ['v1', @schedule], expires_in: @cache_time, race_condition_ttl: 5.seconds do
  json.schedule do
    json.(@schedule, :title, :start, :end_at)
    json.map asset_url(@schedule.map_url)

    json.events @schedule.events do |e|
      json.extract! e, :id, :title, :start, :description, :location_id
      json.teller_ids e.tellers.order('event_teller_schedules.id').map(&:id)

      # The app team is expecting end instead of end_at
      json.end e.end_at
    end

    json.tellers @schedule.tellers do |t|
      json.extract! t, :id, :first_name, :last_name, :phone, :role, :bio, :website

      json.image asset_url(t.avatar_url)
    end

    json.locations @schedule.locations, :id, :name

    json.vendors @vendors do |v|
      json.extract! v, :name, :location

      # The app team wants to see the food list named description.
      json.description v.foods_list
    end
  end
end

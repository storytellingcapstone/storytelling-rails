json.array!(@events) do |event|
  json.extract! event, :id, :start, :end_at, :description, :location_id, :schedule_id
  json.url event_url(event, format: :json)
end

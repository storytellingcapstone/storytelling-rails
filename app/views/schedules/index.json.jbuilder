json.array!(@schedules) do |schedule|
  json.extract! schedule, :id, :title, :start, :end_at, :map
  json.url schedule_url(schedule, format: :json)
end

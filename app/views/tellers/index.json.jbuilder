json.array!(@tellers) do |teller|
  json.extract! teller, :id, :first_name, :last_name, :website, :role, :bio, :phone, :name
  json.url teller_url(teller, format: :json)
end

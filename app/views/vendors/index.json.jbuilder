json.array!(@vendors) do |vendor|
  json.extract! vendor, :id, :name, :location, :foods_list, :status
  json.url vendor_url(vendor, format: :json)
end

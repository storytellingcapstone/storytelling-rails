# config valid only for current version of Capistrano
lock '3.4.0'

set :application, 'storytelling-admin'

# Uncomment and set the appropriate repository url below.
# Must be accessible from the internet.
# The user deploying the site must be authorized to access the repository.
set :repo_url, 'git@bitbucket.org:jjarman/storytellingonrails.git'

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp





#### You should not need to change anything below this line ####
################################################################

# Default deploy_to directory is /var/www/my_app_name
# set :deploy_to, '/var/www/my_app_name'

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/secrets.yml')
set :linked_files, fetch(:linked_files, []).push('config/secrets.yml')

# Default value for linked_dirs is []
# set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system')
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system', 'public/uploads')

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

namespace :deploy do

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

  # Supply CAP_COLD=yes when running deploy
  # to perform any tasks necessary for initial
  # setup.
  # E.g. Setup the DB and Start Passenger
  # Inspired by: http://stackoverflow.com/a/25098985
  if ENV['CAP_COLD'] == 'yes'
    before :compile_assets, :setup_db => [:set_rails_env] do
      on primary(:db) do
        within release_path do
          with rails_env: (fetch(:rails_env) || fetch(:stage)) do
            execute :rake, 'db:setup'
          end
        end
      end
    end

    # Start passenger right before the passenger:restart task
    # tries to run. Otherwise, the whole deploy fails.
    before "passenger:restart", :start_passenger do
      on roles(:app) do
        # Don't try to start passenger if it's already running.
        # This would cause the deploy to fail.
        # The passenger:restart task will take care of restarting it.
        unless test("service storytelling-admin status | grep -q running")
          execute "sudo service storytelling-admin start"
        end
      end
    end
  end

  desc "Add an admin user (bundle exec cap production 'deploy:create_admin[user@example.com,password]')"
  task :create_admin, [:email, :password] => [:set_rails_env] do |task, args|
    on primary(:app) do
      within current_path do
        with rails_env: (fetch(:rails_env) || fetch(:stage)) do
          execute :rake, "db:create_admin[#{args[:email]},#{args[:password]}]"
        end
      end
    end
  end

  namespace :import_csv do
    desc "Import tellers from a CSV"
    task :tellers, [:file] => [:set_rails_env] do |task, args|
      on primary(:app) do
        within current_path do
          with rails_env: (fetch(:rails_env) || fetch(:stage)) do
            if ( args.file != "" ) && File.exist?(File.expand_path(args.file))
              csv_file = '/tmp/import_tellers.csv'
              upload! File.expand_path(args.file), csv_file
              execute :rake, "db:import_csv:tellers[#{csv_file}]"
            else
              raise "You must provide a valid file to import."
            end
          end
        end
      end
    end
  end

end

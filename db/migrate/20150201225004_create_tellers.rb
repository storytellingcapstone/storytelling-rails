class CreateTellers < ActiveRecord::Migration
  def change
    create_table :tellers do |t|
      t.string :first_name
      t.string :last_name
      t.string :website
      t.string :role
      t.text :bio
      t.string :phone

      t.timestamps null: false
    end
  end
end

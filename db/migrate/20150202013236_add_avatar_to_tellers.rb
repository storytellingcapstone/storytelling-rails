class AddAvatarToTellers < ActiveRecord::Migration
  def change
    add_column :tellers, :avatar, :string
  end
end

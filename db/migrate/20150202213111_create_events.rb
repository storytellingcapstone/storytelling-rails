class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.datetime :start
      t.datetime :end
      t.text :description
      t.references :location, index: true
      t.references :schedule, index: true

      t.timestamps null: false
    end
    add_foreign_key :events, :locations
    add_foreign_key :events, :schedules
  end
end

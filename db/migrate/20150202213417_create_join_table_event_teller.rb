class CreateJoinTableEventTeller < ActiveRecord::Migration
  def change
    create_join_table :events, :tellers do |t|
      t.index [:event_id, :teller_id]
      t.index [:teller_id, :event_id]
    end
  end
end

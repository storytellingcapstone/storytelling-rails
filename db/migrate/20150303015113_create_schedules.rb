class CreateSchedules < ActiveRecord::Migration
  def change
    create_table :schedules do |t|
      t.string :title
      t.date :start
      t.date :end
      t.string :map

      t.timestamps null: false
    end
  end
end

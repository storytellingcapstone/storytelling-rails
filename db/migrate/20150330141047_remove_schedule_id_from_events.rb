class RemoveScheduleIdFromEvents < ActiveRecord::Migration
  def change
    remove_reference :events, :schedule, index: true
    remove_foreign_key :events, :schedules
  end
end

class RenameSchedulesEndToEndAt < ActiveRecord::Migration
  def change
    rename_column :schedules, :end, :end_at
  end
end

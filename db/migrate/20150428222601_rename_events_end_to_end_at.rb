class RenameEventsEndToEndAt < ActiveRecord::Migration
  def change
    rename_column :events, :end, :end_at
  end
end

class RemoveJoinTableEventTeller < ActiveRecord::Migration
  def change
    drop_join_table :events, :tellers do |t|
      t.index [:event_id, :teller_id]
      t.index [:teller_id, :event_id]
    end
  end
end

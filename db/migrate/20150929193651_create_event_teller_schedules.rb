class CreateEventTellerSchedules < ActiveRecord::Migration
  def change
    create_table :event_teller_schedules do |t|
      t.references :event, index: true
      t.references :teller, index: true
      t.timestamps null: false
    end
    add_foreign_key :event_teller_schedules, :events
    add_foreign_key :event_teller_schedules, :tellers
  end
end

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).

# This seed data is only created in production.
# It should not contain sensitive information such as
# passwords.
#
# If you choose to add a default account here, you must
# change its password immediately upon provisioning.
# Otherwise, you risk the site being compromised and
# defamed by anyone who manages to find the source
# or guess a simple password.
if Rails.env.production?
  # Locations
  [
    {
      name: "Courthouse Tent"
    },
    {
      name: "College Street Tent"
    },
    {
      name: "Creekside Tent"
    },
    {
      name: "Library Tent"
    },
    {
      name: "Mill Spring Park"
    },
    {
      name: "Tent on the Hill"
    },
    {
      name: "International Storytelling Center Theater"
    }
  ].each do |l|
    locations << Location.create_with(l).find_or_create_by(name: l[:name])
  end
end

# This seed data is only created in development.
# It should be safe to include "senstive" information
# here as long as it's not used in production. E.g.
# It's safe to create an account with the password being
# "password", because you will not use that as your password
# in production.
if Rails.env.development?
  locations = []
  tellers = []
  events = []
  vendors = []

  # Admins
  [
    {
      email: "admin@example.com",
      password: "password"
    }
  ].each do |a|
    Admin.create_with(a).find_or_create_by(email: a[:email])
  end

  # Schedule
  # Note: The dates for the schedule are parsed in ruby time (d/m/y instead of
  # the US m/d/y). We may need to override this in the schedule model.
  Schedule.create_with(
    title: "Main Schedule",
    start: "1/5/2015 8:00 am",
    end_at: "3/5/2015 7:00 pm",
    map: "somemap.jpg"
  ).find_or_create_by(title: "Main Schedule")

  # Locations
  [
    {
      name: "Courthouse Tent"
    },
    {
      name: "College Street"
    },
    {
      name: "Library Tent"
    },
    {
      name: "Mill Spring Park"
    }
  ].each do |l|
    locations << Location.create_with(l).find_or_create_by(name: l[:name])
  end

  # Tellers
  [
    {
      first_name: "Carol",
      last_name: "Birch",
      website: "http://www.carolbirchstoryteller.com/",
      role: "Featured Teller",
      bio: "Carol Birch's style revitalizes language, her art is an absence of artifice, and the stories she tells offer memories worth keeping. A storyteller and award-winning recording artist, Birch is known for a compelling blend of energy, warmth, vulnerability and directness. In her sure voice, distinguished literature emerges as intimate, conversational and delectable. Birch has shared her work around the world in Singapore, Australia, and Europe; her media appearances include NPR and ABC's Nightline. An NSN ORACLE Circle of Excellence award recipient, Birch teaches at Southern Connecticut State University. An articulate, skillful weaver of images and creator of moods... -Booklist Magazine",
      phone: "(423) 555-1212",
      avatar: ""
    },
    {
      first_name: "Bobby",
      last_name: "Norfolk",
      website: "http://folktale.com/",
      role: "Featured Teller",
      bio: "Internationally known teaching artist, actor, TV host, recording artist, and professional storyteller since 1975, Bobby Norfolk's unique life path is reflected in his humorous and animated stories. With tales rich in creativity, lively sound effects, high energy and vibrant 3D characters, his performances promote cultural diversity, self-esteem and character education. Norfolk has recorded multiple storytelling CDs which have won ten prestigious Parent's Choice Gold Awards, and has co-authored eight award-winning children's books. Norfolk has also received three Emmy awards as host of CBS's television series, Gator Tales, and is an NSN ORACLE Circle of Excellence award recipient. Like an adventure story come to life! -St. Louis Post Dispatch",
      phone: "(423) 555-1289",
      avatar: ""
    }
  ].each do |t|
    tellers << Teller.create_with(t).find_or_create_by(first_name: t[:first_name], last_name: t[:last_name])
  end

  # Vendors
  [
    {
      name: "Ryan's Pizza",
      location: "Near the Creekside Tent",
      foods_list: "Pizza, pizza, and more pizza.",
      status: "inactive"
    },
    {
      name: "Byron's Ice Cream Truck",
      location: "Roaming around the city",
      foods_list: "Rocky road, chocolate chip cookie dough, bacon and brownie, the list goes on!",
      status: "inactive"
    },
    {
      name: "Pushpita's Momos",
      location: "Dinner Tent",
      foods_list: "Chicken Momos",
      status: "inactive"

    },
     {
      name: "Dunkin Donuts",
      location: "Park Food Court",
      foods_list: "Donuts",
      status: "active"

    }
  ].each do |v|
    vendors << Vendor.create_with(v).find_or_create_by(name: v[:name])
  end

  # Events
  [
    {
      title: "Storytelling Sampler",
      start: "5/1/2015 12:35 pm",
      end_at: "5/1/2015 1:35 pm",
      description: "Some awesome stories and people",
      location_id: "#{locations[0].id}",
      user_input_teller_ids: "#{tellers[0].id},#{tellers[1].id}"
    },
    {
      title: "Family Showcase",
      start: "5/1/2015 12:35 pm",
      end_at: "5/1/2015 2:35 pm",
      description: "Event 1",
      location_id: "#{locations[2].id}",
      user_input_teller_ids: "#{tellers[0].id},#{tellers[1].id}"
    },
    {
      title: "Steinbeck Out Loud!",
      start: "5/2/2015 3:35 pm",
      end_at: "5/2/2015 5:35 pm",
      description: "Event 2",
      location_id: "#{locations[1].id}",
      user_input_teller_ids: "#{tellers[0].id}"
    }
  ].each do |e|
    events << Event.create_with(e).find_or_create_by(title: e[:title], description: e[:description])
  end
end

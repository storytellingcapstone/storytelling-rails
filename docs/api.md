# API documentation

## How it works

When an HTTP GET request is made to **/api/schedule/sync.json**, the schedule with the ending the farthest in the future is found, all active Vendors are retrieved, and a JSON response is built.
The response includes the Schedule metadata (start, end, map image, etc.), all Events that are occurring in that Schedule with their associated metadata (location id, title, description, teller id's, start and end), Vendors (name, location and description), Locations (name and id), and all Tellers that are associated with Events in the Schedule (website, phone number, image, first and last name, role, id and bio).
This response is then returned to the client.

All image url's are valid and can be directly accessed to return the appropriate image.

### Caching

The API response is currently cached for **10 minutes** to improve performance under load.
This can be configured in **app/controllers/api/schedule\_controller.rb**

## Example Response

```json
{
   "schedule" : {
      "locations" : [
         {
            "name" : "Courthouse",
            "id" : 5
         },
         {
            "name" : "Tent on the Hill",
            "id" : 6
         }
      ],
      "vendors" : [
         {
            "location" : "Park Food Court",
            "name" : "Dunkin Donuts",
            "description" : "Donuts"
         }
      ],
      "map" : "http://localhost:3000/uploads/schedule/map/3/2014_Festival_Map.png",
      "events" : [
         {
            "location_id" : 5,
            "title" : "STORYTELLING SAMPLER",
            "id" : 332,
            "teller_ids" : [
               52,
               53
            ],
            "description" : "",
            "end" : "2014-10-03T11:00:00.000Z",
            "start" : "2014-10-03T10:00:00.000Z"
         },
         {
            "location_id" : 5,
            "title" : "STORYTELLING SAMPLER",
            "id" : 333,
            "teller_ids" : [
               55,
               60
            ],
            "description" : "",
            "end" : "2014-10-03T12:30:00.000Z",
            "start" : "2014-10-03T11:30:00.000Z"
         },
         {
            "location_id" : 5,
            "title" : "STORYTELLING SAMPLER",
            "id" : 334,
            "teller_ids" : [
               44,
               54
            ],
            "description" : "",
            "end" : "2014-10-03T14:00:00.000Z",
            "start" : "2014-10-03T13:00:00.000Z"
         }
      ],
      "tellers" : [
         {
            "website" : "http://www.ddavisstoryteller.com/",
            "phone" : "12529952603",
            "image" : "",
            "last_name" : "Davis",
            "id" : 52,
            "role" : "Featured Teller",
            "bio" : "Donald Davis was born in a Southern Appalachian mountain world rich in stories, surrounded by a family of traditional storytellers who told him gentle fairy tales, simple and silly Jack tales, scary mountain lore, ancient Welsh and Scottish folktales, and most importantly, nourishing, true-to-life stories of his own neighbors and kin. \n\nFeatured at festivals throughout the U.S. and world, Davis is also known as a prolific author, producer of books and CDs, and as a guest host for NPR's Good Evening.  Davis is a recipient of the NSN ORACLE Circle of Excellence and Lifetime Achievement awards. \n\nI could have listened all morning to Donald Davis' his stories often left listeners limp with laughter at the same time they struggled with a lump in the throat.  -New York Times",
            "first_name" : "Donald"
         },
         {
            "website" : "http://doviethomason.com/",
            "phone" : "13017522967",
            "image" : "",
            "last_name" : "Thomason",
            "id" : 53,
            "role" : "Featured Teller",
            "bio" : "Dovie Thomason, a beloved First Nations storyteller, began her journey as a storyteller with the childhood influence of her Kiowa Apache grandmother, Dovie.  Thomason's interpretations of her Kiowa Apache and Lakota ancestors' stories, as well as her original stories, have received international acclaim.\n\nA tradition bearer, writer, teacher and storyteller, she is a multi-talented speaker whose performances are both elegant and witty.  Sometimes wry, occasionally rueful, and always enchanting, Thomason has shared her tales throughout North America and extensively overseas.  She is an NSN ORACLE Circle of Excellence award recipient. \n\nThomason is an outstanding teller with a strong, lyrical, dignified voice.  -School Library Journal",
            "first_name" : "Dovie"
         },
         {
            "website" : "http://www.kevinkling.com/",
            "phone" : null,
            "image" : "",
            "last_name" : "Kling",
            "id" : 55,
            "role" : "Featured Teller",
            "bio" : "Best known for his popular commentaries on NPR's All Things Considered and his storytelling stage shows, Kevin Kling delivers hilarious, often tender, stories.  He's from Minneapolis, where he lives on purpose.  Says Kling, When you freeze paradise, it lasts a little longer.  His autobiographical tales are as fascinating as they are true to life: hopping freight trains, getting hit by lightning, and growing up in Minnesota.  \n\nKling has performed all over the U.S. and abroad in Europe, Australia and Thailand, and has been awarded numerous grants and fellowships including from the National Endowment for the Arts.  He is an NSN ORACLE Circle of Excellence award recipient.\n\nPart funny guy, part poet and playwright, part wise man.  \n-Krista Tippett, American Public Media host ",
            "first_name" : "Kevin"
         },
         {
            "website" : "http://www.campstreetcafe.com/guy&pipp.htm",
            "phone" : "19366367165",
            "image" : "",
            "last_name" : "Gillette",
            "id" : 60,
            "role" : "New Voice",
            "bio" : "With a lifelong interest in the history of the West and its music, Pipp Gillette plays traditional cowboy music on guitar, banjo, harmonica and bones.  He has performed across the U.S. and abroad, including the Smithsonian Folklife Festival, the Cowboy Poetry Gathering, and tours in England and Japan. \n\nAs the Gillette Brothers, Pipp and his late brother Guy recorded eight albums of traditional cowboy music and were recipients of the Western Heritage Wrangler Award for Outstanding Traditional Album of 2010 as well as the 2012 Wrangler Award for Outstanding Original Western Composition.  Gillette lives in Texas, where he runs a live music venue, the Camp Street Cafe and raises cattle on the family ranch started in 1912. \n\nIt's rare to get to hear such a quality performance with so many layers.  -A.W. Perry Homestead Museum\n",
            "first_name" : "Pipp"
         },
         {
            "website" : "http://leppstorytelling.com/",
            "phone" : "13047442126",
            "image" : "",
            "last_name" : "Lepp",
            "id" : 44,
            "role" : "Featured Teller",
            "bio" : "Bil Lepp is an internationally-known storyteller, author, and recording artist whose style has been described as a satisfying blend of Bill Cosby and Jeff Foxworthy.  A five-time champion of the West Virginia Liars Contest, his outrageous tall-tales and witty stories are a signature of his repertoire. \n\nLepp has been featured at venues across the nation, including the Smithsonian Folklife Festival and a recent appearance for Comedy Central.  He is the recipient of the NSN ORACLE Circle of Excellence award, and his picture book, The King of Little Things, won a Parent's Choice Gold Award. \n\nWhen [he] wraps up his story in one more burst of implausibility, the audience leaps to its feet, cheering.  -The Dallas Morning News",
            "first_name" : "Bil"
         },
         {
            "website" : "http://www.katecampbell.com/",
            "phone" : "16153366668",
            "image" : "",
            "last_name" : "Campbell",
            "id" : 54,
            "role" : "New Voice",
            "bio" : "As the daughter of a Mississippi Baptist preacher, Kate Campbell's formative years were spent in the very core of the civil rights movement of the '60s, and the indelible experiences of those years have shaped her heart, character, and convictions. \n\nCampbell's endearing clear-water vocal delivery and easy command of a full range of American music styles have earned her critical acclaim, and her eloquent gift for storytelling has drawn repeated comparison to such bastions of the Southern writing tradition as Flannery O'Connor and Eudora Welty.  Her appearances span the U.S. and abroad, including the Cambridge Folk Festival, and she's been featured on NPR's Morning Edition and All Things Considered. \n\nYou need this woman's music in your life.  -Music Row Magazine",
            "first_name" : "Kate"
         }
      ],
      "title" : "2014 Schedule",
      "end_at" : "2014-10-05",
      "start" : "2014-10-03"
   }
}
```

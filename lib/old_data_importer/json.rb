##
# This is for importing schedule data from the original schema of this project
# before it was migrated to Rails. It should not be relied on. It has been
# implemented to avoid re-entering all of that data while testing. It is very
# closely tied to a specific JSON export of the data. This particular format
# should be considered deprecated.

class OldDataImporter
  class JSON
    ##
    # Takes IO's pointing to a JSON representation for the events,
    # events_tellers, and tellers entities along with the start date for
    # the schedule and supports importing them into the database..
    def initialize(events_io, events_tellers_io, tellers_io, schedule_start_date)
      @events_io = events_io
      @events_tellers_io = events_tellers_io
      @tellers_io = tellers_io
      @schedule_start_date = schedule_start_date

      @teller_mapper = {}
      @event_mapper = {}
    end

    ##
    # Import the records in the JSON documents.
    # Returns import failures
    def import
      failures = []
      ##
      # Create tellers first so they can be found
      # when assigning them to events.
      each_teller do |t|
        if t.valid?
          t.save
        else
          failures << "Teller: " + t.inspect + "\n" + t.errors.full_messages.join(" | ")
        end
      end

      ##
      # Go ahead and create events.
      each_event do |e|
        if e.valid?
          e.save
        else
          failures << "Event:" + e.inspect + "\n" + e.errors.full_messages.join(" | ")
        end
      end

      updated_events = Set.new

      each_event_teller do |event, teller|
        if teller.valid?
          unless event.tellers.exists? teller.id
            event.tellers << teller
            updated_events.add(event)
          end
        end
      end

      updated_events.each { |e| e.save }

      failures
    end

    ##
    # Iterate through tellers yielding a new Teller object
    # for each teller found. Tellers are not automatically
    # persisted.
    # We try hard not to duplicate tellers. This means
    # returning the existing version from the database
    # if there is one.
    def each_teller
      tellers.each do |t|
        attributes = {
          first_name: t["teller_first_name"],
          last_name: t["teller_last_name"],
          website: t["teller_website"],
          role: t["teller_role"],
          bio: t["teller_bio"],
        }

        teller = Teller.where(attributes).first_or_initialize do |record|
          record.phone = t["teller_phone"]
        end


        if teller.valid?
          @teller_mapper[t["teller_id"]] = teller
        end

        yield teller
      end
    end

    ##
    # Iterate through events yielding a new Event object
    # for each event found. Events are not automatically
    # persisted. However, locations are automatically created
    # and persisted as part of building events, so this method
    # is not side-effect free.
    # We try hard not to duplicate events. This means returning
    # the existing version in the database if there is one.
    def each_event
      events.each do |e|
        event_title = e["event_title"] || e["event_desc"]

        # These attributes need to be set
        # on a new Event
        attributes = {
          description: e["event_desc"],
          title: event_title
        }

        event = nil

        # Get the location now so that
        # we can use it to see if the event
        # already exists
        if e["location_id"].present?
          l = location_from_id(e["location_id"])
        end

        if e["start_time"].present? && e["end_time"].present?
          # Build up our conditions to find an event.
          # This is to attempt to avoid creating duplicates.
          find_conditions = {
            start: DateTime.strptime(event_time_to_us(e["start_time"], e["day_id"], @schedule_start_date), "%m/%d/%Y %I:%M %p").change(offset: "+0000"),
            end_at: DateTime.strptime(event_time_to_us(e["end_time"], e["day_id"], @schedule_start_date), "%m/%d/%Y %I:%M %p").change(offset: "+0000"),
            title: event_title
          }

          # Conditionally add location to the set of find
          # conditions. We don't want to add it if we don't
          # have a location for some reason.
          # Same for description
          find_conditions.tap do |c|
            c[:location] = l if l
            c[:description] = e["event_desc"] if e["event_desc"]
          end

          event = Event.find_by(find_conditions)
        end

        # If the event wasn't found above,
        # create a new one with the appropriate attributes.
        event ||= Event.new(
          attributes.merge(
            start: event_time_to_us(e["start_time"], e["day_id"], @schedule_start_date),
            end_at: event_time_to_us(e["end_time"], e["day_id"], @schedule_start_date)
          )
        )

        # Set the event's location if it is different than before.
        unless event.location && event.location.name == l.name
          event.location = l
        end

        @event_mapper[e["event_id"]] ||= event

        yield event
      end
    end

    ##
    # Iterates through the event-teller relationships.
    # Uses @event_mapper and @teller_mapper to return
    # a tuple of [Event, Teller] where the Teller belongs
    # to the Event. Requires that each_event and each_teller
    # have been run to populate the mappers.
    def each_event_teller
      # If either mapper hasn't been populated yet,
      # call the appropriate iterator(s) for their side
      # effects.
      if @teller_mapper.empty?
        each_teller { }
      end

      if @event_mapper.empty?
        each_event { }
      end

      events_tellers.each do |et|
        # Skip to the next record if an id isn't found for some reason.
        teller = @teller_mapper[et["teller_id"]] or next 
        event = @event_mapper[et["event_id"]] or next

        yield event, teller
      end
    end

    ##
    # Use an Event's time and day id, and the start date
    # of the festival to determine the actual date and time
    # of an event. This is returned in US format which is expected
    # by the Event model.
    def event_time_to_us(time, day_id, start_date)
      midnight = true if time === "12:00 AM"

      # Parse the date and increment based on the given day id
      # Also add a day to account for ending at midnight
      event_date = Date.parse(start_date) + DAY_ID_TO_INCREMENT[day_id].days + (midnight ? 1 : 0)

      # Return the US formatted date and time of the event
      "#{event_date.strftime("%m/%d/%Y")} #{time}"
    end

    ##
    # Uses the id from the old database
    # to find or create a location in the 
    # new one.
    def location_from_id(id)
      Location.find_or_create_by(name: LOCATION_ID_TO_NAME[id])
    end

    def event_count
      events.count
    end

    def teller_count
      tellers.count
    end

    private

    ##
    # These are the relevant day id's
    # taken from the day export.
    # Each id maps to a day increment
    # that can be used to get the actual
    # date of the event based on the first
    # day of the festival as given in the schedule.
    # This is necessary because events now store DateTimes
    # instead of a Time and a reference to a day name.
    DAY_ID_TO_INCREMENT = {
      5 => 0,   # Friday
      6 => 1,   # Saturday
      7 => 2,   # Sunday
      12 => 0,  # Friday - Special
      13 => 1,  # Saturday - Special
      14 => 2   # Sunday - Special
    }

    ##
    # Like the days, the location id's are based on the
    # old database. They have to be translated to the 
    # new one.
    LOCATION_ID_TO_NAME = {
      1 => "Courthouse",
      2 => "Tent on the Hill",
      3 => "Creekside",
      4 => "College Street",
      5 => "Library",
      6 => "Mill Spring Park",
      7 => "Theater"
    }

    def cache
      @c ||= Hash.new
    end

    def events(reload=false)
      if reload || ! cache.key?(:events)
        cache[:events] = ::JSON.load(@events_io)
      end

      cache[:events]
    end

    def events_tellers(reload=false)
      if reload || ! cache.key?(:events_tellers)
        cache[:events_tellers] = ::JSON.load(@events_tellers_io)
      end

      cache[:events_tellers]
    end

    def tellers(reload=false)
      if reload || ! cache.key?(:tellers)
        cache[:tellers] = ::JSON.load(@tellers_io)
      end

      cache[:tellers]
    end
  end
end

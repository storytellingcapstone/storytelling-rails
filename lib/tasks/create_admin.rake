namespace :db do
  desc "Add an admin user (bundle exec rake 'db:create_admin[user@example.com,password]')"
  task :create_admin, [:email, :password] => :environment do |task, args|
    unless (args.email.present? && args.password.present?)
      puts "Error! you must provide an email and password."
      raise "Missing arguments call like: bundle exec rake 'db:create_admin[me@example.com,MySUpeRSecRETPassWOrD]'"
    end

    Admin.create!(
      email: args.email,
      password: args.password
    )

    puts "An account for #{args.email} has been created. Don't forget to change the password!"
  end
end

require_relative "../old_data_importer"

namespace :db do
  namespace :import_json do
    desc "Use with caution: Import old schedule from JSON documents."
    task :old_schedule, [:start_date, :events, :events_tellers, :tellers] => :environment do |task, args|
      [:events, :events_tellers, :tellers].each do |arg|
        unless (args[arg].present? && File.exist?(File.expand_path(args[arg])))
          raise "Error! You must provide a valid JSON file for #{arg.to_s}."
        end
      end

      unless (args.start_date.present?)
        raise "Error! You must provide the start date of the old schedule."
      end

      importer = OldDataImporter::JSON.new(
        File.read(File.expand_path(args.events)),
        File.read(File.expand_path(args.events_tellers)),
        File.read(File.expand_path(args.tellers)),
        args.start_date
      )
      failures = importer.import

      if failures.size > 0 
        puts "There were #{failures.size} errors. They were:"
        puts %Q{#{failures.join("\n")}}
      end
    end
  end
end

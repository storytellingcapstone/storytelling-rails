require_relative "../teller_importer"

namespace :db do
  namespace :import_csv do
    desc "Import tellers from a CSV."
    task :tellers, [:file] => :environment do |task, args|
      unless (args.file.present? && File.exist?(File.expand_path(args.file)))
        raise "Error! You must provide a valid CSV file."
      end

      csv = TellerImporter::CSV.new(File.read(File.expand_path(args.file)))
      records_imported, failures = csv.import

      puts "#{records_imported} records were imported successfully."
      if failures.size > 0 
        puts "There were #{failures.size} errors. They were:"
        puts %Q{#{failures.join("\n")}}
      end
    end
  end
end

require 'csv'

class TellerImporter
  class CSV
    # Takes an IO that represents a CSV
    def initialize(csv)
      @csv = csv
    end

    # Yields Teller objects created from the CSV
    def each_teller
      csv.each do |row|
        yield Teller.new(
          first_name: row["First Name"],
          last_name: row["Last Name"],
          website: row["Website"],
          role: row["Role"],
          bio: row["Bio"],
          phone: row["Phone"]
        )
      end

      csv.rewind
    end

    # Import the records in the CSV
    # Returns [number of records imported, import failures]
    def import
      count = 0
      failures = []

      # Wrap in a transaction which should be faster
      Teller.transaction do
        each_teller do |teller|
          count = count + 1
          if teller.valid?
            teller.save
          else
            failures << "Teller: " + teller.inspect + "\n" + teller.errors.full_messages.join(" | ")
          end
        end
      end

      [count, failures]
    end

    # Count the records in the CSV.
    # Loops through the entire CSV to get the count.
    # This result is cached, pass true to refresh the count.
    def count(refresh=false)
      if !@count || refresh
        count = 0
        csv.each { count = count + 1 }

        @count = count
        csv.rewind
      end

      @count
    end

    private
    def csv
      ::CSV.new(@csv, headers: true)
    end
  end
end

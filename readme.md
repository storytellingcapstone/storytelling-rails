# Storytelling on Rails

This is the rails version of the Storytelling admin site and API.

## Developing

**The box url (config.vm.box_url) in the Vagrantfile must point at a valid download location. If this needs to be changed, it should be committed and pushed to the repository so that everyone will be using the same version.**

There should only be a few steps necessary to start developing the site.

1. Download and install [Vagrant](https://www.vagrantup.com/downloads.html)
    * Vagrant will take care of configuring and running a virtual linux box.
1. Clone this repo.
    * SSH: `git@bitbucket.org:OWNERUSERNAMEHERE/storytellingonrails.git`
    * Or HTTPS: `https://bitbucket.org/OWNERUSERNAMEHERE/storytellingonrails.git`
1. Vagrant should have been automatically added to your PATH (https://docs.vagrantup.com/v2/installation/index.html)
1. Make sure the admin info you want to use is in the seed file (`db/seeds.rb`).
1. `cd /path/to/repo`
1. `vagrant up`
1. Go grab something to eat.
    * Vagrant will download the ~500MB VM, configure it, and boot it. (This download only needs to be done once.)
1. Now connect to your machine using Putty (https://github.com/Varying-Vagrant-Vagrants/VVV/wiki/Connect-to-Your-Vagrant-Virtual-Machine-with-PuTTY)
1. Run `cd /vagrant; bundle exec rake db:setup` INSIDE the machine (This only needs to be done the first time)
    * This will setup the database and load the seed data.
1. Run `cd /vagrant; bundle exec rails s --binding=*` INSIDE the machine
  * Stop the webserver with `CTRL-C`
	* See below (Ruby/Rails) for more information about these and more commands
1. Now you can develop the rails app on your local machine and the changes will be reflected in the VM.
	* Some changes (such as configuration) may need to restart the server to see reflected
    * Vagrant configures forwarded ports, so you can access the app at http://localhost:8088
    * It configures a shared folder automatically, so you can edit the code in your preferred Windows editor of choice and changes will be reflected instantly in the app.
    * It takes care of installing all dependencies for the app. Gems, system packages like imagemagick, ruby, etc.

### Useful Commands

If you aren't familiar with Vagrant or Rails, you should read through all of these. They answer many of the common questions you will have.

#### Vagrant
* `vagrant up` Starts the virtual machine and provisions it.
* `vagrant ssh` connects to the VM through ssh.
* `vagrant provision` Runs the provisioner while the machine is running.
* `vagrant halt` Shuts down the virtual machine.
* `vagrant reload` Restarts the machine, taking into account any changes made in the `Vagrantfile`.
* `vagrant destroy` Blows away the machine. Any handmade changes will be lost. However, the base box is still stored on your machine, so when you run `vagrant up` again it won't have to redownload it.
    * Therefore, don't make changes by hand. If it's necessary, we need to script it or repackage the box with the changes.

#### Ruby/Rails
* All rails commands should be executed on the VM (`vagrant ssh`) in the shared directory: `/vagrant`
    * `cd /vagrant`
* Commands are preceded with `bundle exec` to take advantage of Ruby's dependency management system. It ensures that the app only loads the specified library versions.
    * If it becomes an issue, it can easily be aliased to `be` with `alias be="bundle exec"`
* `bundle exec rails s --binding=*` Starts the development server on port 3000. This is accessible at 8088 on your host. Binding is necessary to allow external connections, such as your host, to connect.
* `bundle exec rails c` Opens a ruby console with all of the app's libraries loaded. It can be used to debug the app or change things like database records.
* `bundle exec rails g migration` Generate a new migration. Use `-h` or check the [Rails migration guide](http://guides.rubyonrails.org/active_record_migrations.html) for the conventions that should be used to greatly speed up creating new migrations.
    * For example `bundle exec rails g migration CreateStudents name:string part_number:string` will automatically add the correct code to the migration for creating the Students table with those columns.
* `bundle exec rails g scaffold` Generate CRUD scaffolding for a given model. Again, take advantage of existing conventions to generate accurate code rather than doing it by hand.
* `bundle exec rails d <migration|scaffold|etc.>` Attempt to reverse changes made by the given generator. You must pass the same arguments here as you did to generate the code. This is basically an undo button for code generation.
    * Be very careful! If you run your migrations and then do this before rolling back the database, it is a pain to correct it. If you generate code, run `bundle exec rake db:migrate`, and realize it's not right, run `bundle exec rake db:rollback` before using this to destroy the generated code. You won't have a problem if you do that.
* `bundle exec rake db:migrate` Once you have created a new migration, have checked it and added any missing columns or indexes, and are ready to run it, this command will apply it to the database.
* `bundle exec rake db:rollback` This command rollsback the last migration. You can then edit the migration and run it again or delete the migration if it was a mistake.
* `bundle install` Installs the libraries specified in the `Gemfile`. This should only be needed if changes are made to it.
    * This is run automatically when you provision the app.

##### Generate ERD
* You can generate an ERD for the current schema.
    * `bundle exec erd --filename=docs/erd`
        * This will generate a PDF formatted ERD of the schema in **docs/erd.pdf**

## More Details
* Ruby version
    * Ruby 2.1.2

* System dependencies
    * Imagemagick

* Configuration

* Database creation
    * `bundle exec rake db:setup`

* Database initialization
    * `bundle exec rake db:seed`
        * This is only needed when changing seed data specifically. Seed data is automatically loaded when you first setup the database (`bundle exec rake db:setup`).

* How to run the test suite
    * `bundle exec rspec`

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions
    1. Make sure that the repo address(**repo_url**) is correct in **config/deploy.rb**.
    1. Make sure that the server address is correct in **config/deploy/production.rb**.
    1. `bundle exec cap production deploy`

* Schedule API
    * Documentation for the Schedule API (used by the Storytelling app) can be found in **docs/api.md**

## Production Setup

### Preparing the server

These instructions assume Ubuntu 14.04 x64 running on DigitalOcean.
DigitalOcean refers to virtual servers as "Droplets".

#### Create the Droplet
1. Login to DigitalOcean
1. Select **Create Droplet**
1. Select the size
    1. At least the $10/mo option is recommended.
1. Leave the default region
1. Select **Enable Backups**
1. Select the Image **Ubuntu 14.04 x64**
1. Add your SSH key.
    1. If you don't know what that means, Google is your friend.
    If you can't use a key, DigitalOcean will email you a password to use for the follwing steps; however, this is far less secure.
1. Finally, select the large **Create Droplet** button at the bottom of the page.

#### Configure the Droplet
Connect to the droplet with SSH.
You will need the droplet's IP address.
It is listed on the [Droplets](https://cloud.digitalocean.com/droplets) page.
This IP address should also be used when configuring DNS for the site.
We assume that the site will be reachable at **app.storytellingcenter.net**.
If this is not the case, you should update the line in **config/deploy/production.rb** accordingly.

* Most people use [PuTTY](http://www.chiark.greenend.org.uk/~sgtatham/putty/download.html) for this on Windows.
* SSH is accessible in the terminal on OS X (Terminal.app) and Linux. `ssh user@host`
* Connect using the following information:
    * user: **root**
    * host: **_the IP address of the droplet_**
    * Make sure to use the key you provided when creating the droplet or the password that DigitalOcean emailed you.

The following commands will be run on the server (using the SSH connection you just made) unless otherwise specified.

##### Update and install system dependencies
1. After connecting to the server, upgrade currently installed packages.
    * `aptitude update && aptitude safe-upgrade`
1. Add additional repositories for required dependencies.
    1. `apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 7FCC7D46ACCC4CF8 # PostgreSQL`
    1. `apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 561F9B9CAC40B2F7 # Phusion Passenger`
    1. `add-apt-repository 'deb http://apt.postgresql.org/pub/repos/apt/ trusty-pgdg main' # PostgreSQL`
    1. `add-apt-repository 'deb https://oss-binaries.phusionpassenger.com/apt/passenger trusty main' # Phusion Passenger`
1. Install necessary packages
    1. `aptitude install apt-transport-https ca-certificates`
    1. `aptitude update && aptitude install imagemagick git postgresql postgresql-contrib libpq-dev passenger autoconf bison build-essential libssl-dev libyaml-dev libreadline6-dev zlib1g-dev libncurses5-dev libffi-dev libgdbm3 libgdbm-dev`

##### Restart the server
It's a good idea to restart the server at this point.
This will disconnect your session.
Wait a couple minutes and then reconnect.

* `reboot`

##### Setup key based access for the root user
If you setup the droplet with a key initially, you can skip to \#2.

1. Add your SSH public key (likely named something like **id_rsa.pub**) to **root**'s **~/.ssh/authorized_keys**.
    1. `mkdir ~/.ssh`
    1. `touch ~/.ssh/authorized_keys`
    1. `chmod 700 ~/.ssh && chmod 600 ~/.ssh/authorized_keys`
    1. Append your SSH key to the **authorized_keys** file.
       This can be done many ways.
       One way is to:
        1. (On your computer) Open your SSH public key in a text editor (e.g. Notepad).
        1. Copy the contents of this file.
        1. (Back on the droplet) paste it into the **authorized_keys** file with the **cat** command.
            * `cat >> ~/.ssh/authorized_keys`
                * Press Enter, then paste, then Ctrl+D
            * Verify that this worked with `cat ~/.ssh/authorized_keys`.
            Your SSH key should be displayed.
1. Disable remote password access to the root user.
   This will reduce the likelihood of the server getting compromised.
    * `sed -i '/^PermitRootLogin/ s/yes/without-password/' /etc/ssh/sshd_config`

##### Disable password access for all users over SSH
This will make sure users can only authenticate with keys.

* `sed -r -i 's/^.?PasswordAuthentication.*/PasswordAuthentication no/' /etc/ssh/sshd_config`

##### Configure the firewall
This will help limit unauthorized connections.

1. Configure SSH (allow it, but rate-limit connection attempts)
    1. `ufw allow ssh`
    1. `ufw limit ssh`
1. Configure HTTP
    * `ufw allow http`
1. Enable the firewall
    * `ufw enable`

##### Configure a user account to deploy and run the app
1. Create the **deploy** user.
This will create a user named **deploy** that cannot be accessed with a password, but can still be accessed with an SSH key.
    * `adduser --disabled-password --gecos "" deploy`
1. Add your SSH public key (likely named something like **id_rsa.pub**) to **deploy**'s authorized keys.
This will allow you to login as the **deploy** user later.
These steps are very similar to the steps for the **root** user.
    1. Change to the **deploy** user.
        * `sudo su - deploy`
    1. Create the **authorized_keys** file that tells SSH to accept your key for the **deploy** user.
        1. `mkdir ~/.ssh`
        1. `touch ~/.ssh/authorized_keys`
    1. Make sure file permissions are correct.
        1. `chmod 700 ~/.ssh`
        1. `chmod 600 ~/.ssh/authorized_keys`
    1. Append your SSH key to the **authorized_keys** file.
    This can be done many ways.
    One way is to:
        1. (On your computer) Open your SSH public key in a text editor (e.g. Notepad).
        1. Copy the contents of this file.
        1. (Back on the droplet) paste it into the **authorized_keys** file with the **cat** command.
            * `cat >> ~/.ssh/authorized_keys`
                * Press Enter, then paste, then Ctrl+D
            * Verify that this worked with `cat ~/.ssh/authorized_keys`.
            Your SSH key should be displayed.

##### Install Ruby
We are going to use [rbenv](https://github.com/sstephenson/rbenv) to handle installing the correct version of Ruby.

1. Make sure you are running as the **deploy** user.
    * `whoami # Should return deploy`
1. Install **rbenv**.
    1. `git clone https://github.com/sstephenson/rbenv.git ~/.rbenv`
    1. `echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc`
    1. `echo 'eval "$(rbenv init -)"' >> ~/.bashrc`
    1. Make sure **rbenv** is available to non-interactive processes.
        * `sed -i '1iexport PATH=~/.rbenv/shims:~/.rbenv/bin:"$PATH"' ~/.bashrc`
    1. Logout and log back in for the changes to take effect.
1. Install **ruby-build**.
    * `git clone https://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build`
1. Create the **rbenv cache** directory.
    * `mkdir ~/.rbenv/cache`
1. Install **Ruby 2.1.2**
    1. `rbenv install 2.1.2`
    1. Grab a coffee while you wait...
1. Set **Ruby 2.1.2** as the default.
    * `rbenv global 2.1.2`
1. Install **Bundler**, Ruby's library manager.
    * `gem install bundler`

##### Configure the database

1. Make sure you are running as **root** (`exit` will accomplish this if you are following along, or logout and back in as **root**)
    * `whoami # Should return root`
1. Postgres won't require a password if we name our database user the same as the user running our app.
    * `sudo -iu postgres createuser deploy --createdb`

##### Configure the deployment directory

1. Make sure you are running as **root** (`exit` will accomplish this if you are following along, or logout and back in as **root**)
    * `whoami # Should return root`
1. Execute the following commands:

```
#!shell
deploy_to=/var/www/storytelling-admin
mkdir -p ${deploy_to}
chown deploy:deploy ${deploy_to}
umask 0002
chmod g+s ${deploy_to}
mkdir ${deploy_to}/{releases,shared}
chown deploy ${deploy_to}/{releases,shared}
```

##### Setup the secrets.yml file that Rails uses to secure cookies.
1. On your development machine, run `bundle exec rake secret` to generate a secure secret key.
1. Copy it.
1. Back on the server, change to the deploy user.
    * `sudo su - deploy`
1. Run the following command and then paste the key and hit Enter.
   This will set a variable without leaving the key in any logs.
    * `read -s -p 'Paste the secret here then press Enter (It will not be displayed in the terminal.): ' secret; echo;`
1. Use this variable to create **secrets.yml**

```
#!shell
mkdir -p /var/www/storytelling-admin/shared/config
cat << EOF > /var/www/storytelling-admin/shared/config/secrets.yml
production:
  secret_key_base: $secret
EOF
```

##### Configure the site to start automatically

1. As **root**, create **/etc/init/storytelling-admin.conf**

```
#!shell
cat <<'EOF' > /etc/init/storytelling-admin.conf
start on runlevel [2345]

env RAILS_ENV=production
env HOME=/home/deploy

script
  export PATH=/home/deploy/.rbenv/shims:/home/deploy/.rbenv/bin:"$PATH"
  passenger start --environment production --user deploy -p 80 /var/www/storytelling-admin/current
end script


pre-stop script
  cd /var/www/storytelling-admin/current
  export PATH=/home/deploy/.rbenv/shims:/home/deploy/.rbenv/bin:"$PATH"
  passenger stop -p 80
end script

respawn
EOF
```

##### Give the deploy user access to start the web server

1. Edit the **sudoers** file (As **root**).
    * `visudo`
        1. Add the following line under `# User privilege specification`
            * `deploy ALL = (ALL) NOPASSWD: /usr/sbin/service storytelling-admin status,/usr/sbin/service storytelling-admin start,/usr/sbin/service storytelling-admin stop`

##### Deploy the site

From your development machine, with your code already having been pushed to BitBucket or wherever is specified in **config/deploy.rb**:

1. Setup the database, install the app, and start the web server.
    * `CAP_COLD=yes bundle exec cap production deploy`
1. Create an intial admin user.
    * `bundle exec cap production 'deploy:create_admin[user@example.com,MyTemporaryPassword]'`
        * Replace **user@example.com** with the email you want to use to login and **MyTemporaryPassword** with a password you will use for you first login.
1. Open the site in your browser and login with the account you just created.
1. Change your password to something you will remember.
1. Create any new users you may need from the web interface.

##### Success

You have successfully deployed the Storytelling Admin Site.
You should not have to repeat these steps again unless you need to setup a brand new server.
Everything from this point should be accomplished from the site itself (adding Vendors, Tellors, Events, etc.).

If you make changes to the code, after you push them to your code repository (e.g. BitBucket), you should be able to simply `bundle exec cap production deploy` to see your changes.

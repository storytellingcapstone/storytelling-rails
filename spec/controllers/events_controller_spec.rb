require 'rails_helper'

RSpec.describe EventsController, type: :controller do

  describe "GET #index" do
    it "returns http success when authenticated" do
      # Create a fake admin user and sign them in
      sign_in Admin.create(email: "test@example.com", password: "password")
      
      expect(subject.current_admin).not_to be_nil
      get :index
      expect(response).to have_http_status(:success)
    end

    it "redirects to the login page when not authenticated" do
      expect(subject.current_admin).to be_nil
      get :index
      expect(response).to redirect_to("/admins/sign_in")
    end
  end

end

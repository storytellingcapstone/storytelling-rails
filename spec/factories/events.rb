FactoryGirl.define do
  factory :event do
    sequence(:title) do |n|
      t = [
        "Storytelling Sampler",
        "OLIO",
        "SHOWCASE",
        "Donald Davis"
      ].sample

      "#{t} ##{n}"
    end

    sequence(:description) { |n| "Event #{n}" }

    trait :random_location do
      location { Location.all.sample }
    end

    trait :random_tellers do
      transient do
        tellers_count 1
      end
      tellers { Teller.all.sample tellers_count }
    end

    factory :event_for_schedule do
      transient do
        schedule_start nil
        schedule_end nil

        # Generate random start and end times between schedule_start and schedule_end.
        # Formula from: http://stackoverflow.com/questions/2410639/best-way-to-create-random-datetime-in-rails
        start_date { Time.at(rand * ( ( schedule_end - 1.hour ).to_f - schedule_start.to_time.to_f) + schedule_start.to_time.to_f) }
        end_date { Time.at(rand * ( schedule_end.to_time.to_f - start_date.to_time.to_f) + start_date.to_time.to_f) }
      end

      start { start_date.to_s(:moment_format) }
      end_at { end_date.to_s(:moment_format) }
    end

    factory :event_with_random_location, traits: [:random_location]
    factory :event_with_random_location_and_tellers, traits: [:random_location, :random_tellers]
    factory :event_for_schedule_with_random_location, traits: [:random_location], parent: :event_for_schedule
    factory :event_for_schedule_with_random_location_and_tellers, traits: [:random_location, :random_tellers], parent: :event_for_schedule

  end
end

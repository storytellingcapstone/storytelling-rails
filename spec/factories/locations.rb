FactoryGirl.define do
  factory :location do
    sequence(:name, ["Courthouse Tent", "Tent on the Hill", "Creekside Tent", "College Street Tent", "Library Tent"].cycle) { |name| "#{name}-#{rand(0..2000)}" }
  end
end

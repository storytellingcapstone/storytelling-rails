FactoryGirl.define do
  factory :schedule do
    sequence(:title) { |n| "Schedule #{n}" }
    start 1.month.from_now
    end_at { start + 1.week }

    # Technique from: http://www.rubydoc.info/gems/factory_girl/file/GETTING_STARTED.md
    factory :schedule_with_events do
      transient do
        events_count 5
        tellers_count 2
      end

      after(:create) do |schedule, evaluator|
        create_list(:event_for_schedule_with_random_location_and_tellers, evaluator.events_count, schedule_start: schedule.start, schedule_end: schedule.end_at, tellers_count: evaluator.tellers_count)
      end
    end
  end
end

FactoryGirl.define do
  factory :teller do
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    website { Faker::Internet.url }
    phone { "(423) #{'%03d' % rand(0..999)}-#{'%04d' % rand(0..9999)}" }
  end
end

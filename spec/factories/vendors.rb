FactoryGirl.define do
  factory :vendor do
    name { Faker::Name.name }
    sequence(:location, ["Park Food Court", "Library Food Court", "Dining Food Court"].cycle) { |name| "#{name}-#{rand(0..2000)}" }
    status { ["inactive", "active"].sample }
  end
end

require 'rails_helper'
require_relative '../support/feature_login'

RSpec.feature "Manage Locations", type: :feature do
  include WithAuthenticatedUser

  let! :locations do
    create_list :location, 5
  end

  def new_location(fields={})
    build :location, fields
  end

  def random_location
    locations.sample(1).first
  end

  def enter_location_details(css_selector, l)
    within css_selector do
      fill_in('Name', with: l.name)
    end
  end

  def go_to_new_location
    visit "/manage/locations"
    click_link "New Location"
    expect(page).to have_text("New Location")
  end

  def edit_location(name)
    visit "/manage/locations"

    within "table" do
      find("tr", text: name).click_link("Edit")
    end

    expect(page).to have_text("Editing Location")
  end

  def edit_random_location
    location = random_location

    edit_location(location.name)

    expect(page).to have_field("Name", with: location.name)

    location
  end

  def expect_management_success(success_text=/was successfully/)
    expect(page).to have_text(success_text)
  end

  def expect_management_failure(failure_text=/\d* error(s)? prohibited this location from being saved/)
    expect(page).to have_text(failure_text)
  end

  def expect_successful_creation
    expect_management_success("Location was successfully created.")
  end

  def expect_failed_creation
    expect_management_failure
  end

  def expect_successful_update
    expect_management_success("Location was successfully updated.")
  end

  def expect_failed_update
    expect_management_failure
  end

  feature 'Create' do
    scenario 'User creates a valid location' do
      go_to_new_location

      enter_location_details "#new_location", new_location

      click_button "Create Location"

      expect_successful_creation
    end

    scenario 'User creates a location with a blank name' do
      go_to_new_location

      enter_location_details "#new_location", new_location(name: "")

      click_button "Create Location"

      expect_failed_creation
    end

    scenario 'User creates a blank location' do
      go_to_new_location

      click_button "Create Location"

      expect_failed_creation
    end
  end

  feature 'Review' do
    scenario 'User views all the locations' do
      visit "/manage/locations"

      expect(page).to have_text("Listing Locations")
      expect(page).to have_link("New Location")

      within "table" do
        locations.each do |l|
          expect(page).to have_text("#{l.name}")
        end
      end
    end

    scenario 'User views a specific location' do
      visit "/manage/locations"
      l = locations.sample(1).first

      within "table" do
        find("tr", text: l.name).click_link("Show")
      end

      expect(page).to have_text("Show Location")
      expect(page).to have_text(l.name)
      expect(page).to have_link("Edit")
    end
  end

  feature 'Update' do
    scenario 'User changes the name of a location' do
      edit_random_location
      n = new_location(name: "Poolside")

      enter_location_details "form[id^=edit_location]", n

      click_button "Update Location"
      
      expect_successful_update
      expect(page).to have_text(/Name:\s+#{Regexp.quote(n.name)}/)
    end

    scenario 'User sets all of the fields of a location to nothing' do
      edit_random_location

      enter_location_details "form[id^=edit_location]", Location.new

      click_button "Update Location"

      expect_failed_update
    end
  end

  feature 'Delete' do
    scenario "User deletes a location" do
      visit "/manage/locations"

      l = random_location

      within "table" do
        find("tr", text: l.name).click_link("Delete")
      end

      expect_management_success("Location was successfully destroyed")
      expect(page.all("tbody tr").count).to eql(locations.count - 1)
    end
  end
end

require 'rails_helper'
require_relative '../support/feature_login'

RSpec.feature "Manage Tellers", type: :feature do
  include WithAuthenticatedUser

  let! :tellers do
    create_list :teller, 5
  end

  def enter_teller_details(css_selector, t)
    within(:css, css_selector) do
      fill_in('First name', with: t.first_name)
      fill_in('Last name', with: t.last_name)
      fill_in('Website', with: t.website)
      fill_in('Role', with: t.role)
      fill_in('Bio', with: t.bio)
      fill_in('Phone', with: t.phone)
    end
  end

  feature "Create" do
    scenario 'User creates a valid teller' do
      visit "/manage/tellers"

      click_link "New Teller"

      expect(page).to have_text("New Teller")

      t = build :teller

      enter_teller_details("#new_teller", t)

      click_button "Create Teller"

      expect(page).to have_text("Teller was successfully created.")
    end

    scenario 'User creates a teller with a blank first name' do
      visit "/manage/tellers"

      click_link "New Teller"

      expect(page).to have_text("New Teller")

      t = build :teller, first_name: ""

      enter_teller_details("#new_teller", t)

      click_button "Create Teller"

      expect(page).to have_text("First name can't be blank")
    end

    scenario 'User creates a teller with a blank last name' do
      visit "/manage/tellers"

      click_link "New Teller"

      expect(page).to have_text("New Teller")

      t = build :teller, last_name: ""

      enter_teller_details("#new_teller", t)

      click_button "Create Teller"

      expect(page).to have_text("Last name can't be blank")
    end

    scenario 'User creates a teller with an invalid phone number' do
      visit "/manage/tellers"

      click_link "New Teller"

      expect(page).to have_text("New Teller")

      t = build :teller, phone: "123-4567"

      enter_teller_details("#new_teller", t)

      click_button "Create Teller"

      expect(page).to have_text("Phone is an invalid phone number")
    end

    scenario 'User creates a blank teller' do
      visit "/manage/tellers"

      click_link "New Teller"

      expect(page).to have_text("New Teller")

      click_button "Create Teller"

      expect(page).to have_text(/Close \d error(s)? prohibited this teller from being saved/)
    end
  end

  feature "Review" do
    scenario 'User views all the tellers' do
      visit "/manage/tellers"

      expect(page).to have_text("Listing Tellers")

      tellers.each do |t|
        expect(page).to have_text("#{t.first_name} #{t.last_name}")
      end
    end

    scenario 'User views a specific teller' do
      visit "/manage/tellers"
      t = tellers.sample(1).first

      within "table.table" do
        find("tr", text: t.phone.phony_formatted(format: '%{ndc}-%{local}', spaces: '-')).click_link("Show")
      end

      expect(page).to have_text("Show Teller")
      expect(page).to have_text(t.first_name)
      expect(page).to have_text(t.last_name)
      expect(page).to have_text(t.bio)
      expect(page).to have_link(t.website)
      expect(page).to have_link("Edit")
    end
  end

  feature "Update" do
    scenario 'User changes the first name of a teller' do
      visit "/manage/tellers"
      t = tellers.sample(1).first

      within "table.table" do
        find("tr", text: /#{t.first_name}\s+#{t.last_name}/).click_link("Edit")
      end

      expect(page).to have_text("Editing Teller")

      value = "John"

      within "form[id^=edit_teller]" do
        fill_in("First name", with: value)
        click_button "Update Teller"
      end

      expect(page).to have_text("Teller was successfully updated")
      expect(page).to have_text(/Name:\s+#{value}\s#{t.last_name}/)
    end

    scenario 'User changes the last name of a teller' do
      visit "/manage/tellers"
      t = tellers.sample(1).first

      within "table.table" do
        find("tr", text: /#{t.first_name}\s+#{t.last_name}/).click_link("Edit")
      end

      expect(page).to have_text("Editing Teller")

      value = "Doe"

      within "form[id^=edit_teller]" do
        fill_in("Last name", with: value)
        click_button "Update Teller"
      end

      expect(page).to have_text("Teller was successfully updated")
      expect(page).to have_text(/Name:\s+#{t.first_name}\s#{value}/)
    end

    scenario 'User changes the phone number of a teller' do
      visit "/manage/tellers"
      t = tellers.sample(1).first

      within "table.table" do
        find("tr", text: /#{t.first_name}\s+#{t.last_name}/).click_link("Edit")
      end

      expect(page).to have_text("Editing Teller")

      value = "423-555-1234"

      within "form[id^=edit_teller]" do
        fill_in("Phone", with: value)
        click_button "Update Teller"
      end

      expect(page).to have_text("Teller was successfully updated")
      expect(page).to have_text(/Phone:\s+#{Regexp.quote(PhonyRails.normalize_number(value, country_code: 'US').phony_formatted(format: :international, spaces: '-'))}/)
    end

    scenario 'User sets all of the fields of a teller to nothing' do
      visit "/manage/tellers"
      t = tellers.sample(1).first

      within "table.table" do
        find("tr", text: /#{t.first_name}\s+#{t.last_name}/).click_link("Edit")
      end

      expect(page).to have_text("Editing Teller")

      within "form[id^=edit_teller]" do
        fill_in("First name", with: "")
        fill_in("Last name", with: "")
        fill_in("Website", with: "")
        fill_in("Role", with: "")
        fill_in("Bio", with: "")
        fill_in("Phone", with: "")
        click_button "Update Teller"
      end

      expect(page).to have_text(/Close \d error(s)? prohibited this teller from being saved/)
    end
  end

  feature "Delete" do
    scenario 'User deletes a teller' do
      visit "/manage/tellers"
      t = tellers.sample(1).first

      expect(page.all("tbody tr").count).to eql(tellers.count)

      within "table.table" do
        find("tr", text: /#{t.first_name}\s+#{t.last_name}/).click_link("Delete")
      end

      expect(page).to have_text("Teller was successfully destroyed")
      expect(page.all("tbody tr").count).to eql(tellers.count - 1)
    end
  end
end

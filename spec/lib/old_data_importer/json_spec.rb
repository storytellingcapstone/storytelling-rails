require_relative '../../../lib/old_data_importer'
RSpec.describe OldDataImporter::JSON do
  subject(:importer) { OldDataImporter::JSON.new(StringIO.new(events), StringIO.new(events_tellers), StringIO.new(tellers), "2014-10-03") }

  let (:events) do
    <<-'EOF'
[
   {
      "end_time" : "11:00 AM",
      "event_id" : 1,
      "start_time" : "10:00 AM",
      "day_id" : 5,
      "location_id" : 1,
      "event_desc" : "",
      "event_title" : "STORYTELLING SAMPLER"
   },
   {
      "end_time" : "12:30 PM",
      "event_id" : 2,
      "start_time" : "11:30 AM",
      "day_id" : 5,
      "location_id" : 1,
      "event_desc" : "",
      "event_title" : "STORYTELLING SAMPLER"
   },
   {
      "end_time" : "2:00 PM",
      "event_id" : 3,
      "start_time" : "1:00 PM",
      "day_id" : 5,
      "location_id" : 1,
      "event_desc" : "",
      "event_title" : "STORYTELLING SAMPLER"
   }
]
    EOF
  end

  let (:events_tellers) do
    <<-'EOF'
[
   {
      "event_id" : 1,
      "teller_order" : 2,
      "teller_id" : 9
   },
   {
      "event_id" : 1,
      "teller_order" : 1,
      "teller_id" : 10
   },
   {
      "event_id" : 2,
      "teller_order" : 2,
      "teller_id" : 12
   },
   {
      "event_id" : 2,
      "teller_order" : 1,
      "teller_id" : 17
   },
   {
      "event_id" : 3,
      "teller_order" : 2,
      "teller_id" : 1
   },
   {
      "event_id" : 3,
      "teller_order" : 1,
      "teller_id" : 11
   }
]
    EOF
  end

  let (:tellers) do
    <<-'EOF'
[ 
  {
      "teller_last_name" : "Lepp",
      "teller_first_name" : "Bil",
      "teller_email" : "booking@leppstorytelling.com",
      "teller_id" : 1,
      "teller_phone" : "304-744-2126",
      "teller_image" : "Bil_Lepp.jpg",
      "teller_bio" : "Bil Lepp is an internationally-known storyteller, author, and recording artist whose style has been described as a satisfying blend of Bill Cosby and Jeff Foxworthy.  A five-time champion of the West Virginia Liars Contest, his outrageous tall-tales and witty stories are a signature of his repertoire. \n\nLepp has been featured at venues across the nation, including the Smithsonian Folklife Festival and a recent appearance for Comedy Central.  He is the recipient of the NSN ORACLE Circle of Excellence award, and his picture book, The King of Little Things, won a Parent's Choice Gold Award. \n\nWhen [he] wraps up his story in one more burst of implausibility, the audience leaps to its feet, cheering.  -The Dallas Morning News",
      "teller_website" : "http://leppstorytelling.com/",
      "teller_role" : "Featured Teller"
   },
   {
      "teller_last_name" : "Harley",
      "teller_first_name" : "Bill",
      "teller_email" : "debbie@billharley.com",
      "teller_id" : 2,
      "teller_phone" : "508-336-9703",
      "teller_image" : "Bill_Harley.jpg",
      "teller_bio" : "Bill Harley is a two-time Grammy award-winning singer-songwriter, author and monologist.  He is known for his trademark blend of song and story, wit and wisdom, and celebrations of commonality and humanity. \n\nA longtime commentator for NPR's news program All Things Considered, Harley is the recipient of both a lifetime achievement award from the Rhode Island Council for the Humanities and the NSN ORACLE Circle of Excellence award.  He tours nationwide as an author, performing artist and keynote speaker.  His latest endeavors include a new book series about 4th grader Charlie Bumpers and a family opera entitled Weedpatch. \n\nHarley delivers an uninhibited performance with wry sense of humor and a love of life we can all share.  -Los Angeles Times",
      "teller_website" : "http://www.billharley.com/",
      "teller_role" : "Featured Teller"
   },
   {
      "teller_last_name" : "Deedy",
      "teller_first_name" : "Carmen",
      "teller_email" : "DeedyBooking@gmail.com",
      "teller_id" : 5,
      "teller_phone" : "404-333-2260",
      "teller_image" : "Carmen_Deedy.jpg",
      "teller_bio" : "Carmen Deedy is a storyteller and award-winning childrens-book author. Born in Cuba, she moved to the U.S. as a child, and her childhood and family provide a rich vein of material for her vividly told stories. Deedy is aNew York Times best-selling author of ten childrens books and hosts Georgia's three-time Emmy-winning children's program, Love That Book.  \n\nDeedy has been an invited speaker at venues as varied as the Smithsonian Institute, TED, and the Kennedy Center, and is a contributor to NPR.  She is an NSN ORACLE Circle of Excellence award recipient.   \n\nWhenever Carmen tells stories, this Americanized   Cuban Comet leaves a bright trail of laughter and healing.  Kathryn Windham",
      "teller_website" : "http://carmenagradeedy.com/",
      "teller_role" : "Featured Teller"
   },
   {
      "teller_last_name" : "Birch",
      "teller_first_name" : "Carol",
      "teller_email" : "carolbirch@gmail.com",
      "teller_id" : 6,
      "teller_phone" : "203-264-3800",
      "teller_image" : "Carol_Birch.jpg",
      "teller_bio" : "Carol Birch's style revitalizes language, her art is an absence of artifice, and the stories she tells offer memories worth keeping.  A storyteller and award-winning recording artist, Birch is known for a compelling blend of energy, warmth, vulnerability and directness. In her sure voice, distinguished literature emerges as intimate, conversational and delectable. \n\nBirch has shared her work around the world in Singapore, Australia, and Europe; her media appearances include NPR and ABC's Nightline.  An NSN ORACLE Circle of Excellence award recipient, Birch teaches at Southern Connecticut State University. \n\nAn articulate, skillful weaver of images and creator of moods...  -Booklist Magazine",
      "teller_website" : "http://www.carolbirchstoryteller.com/",
      "teller_role" : "Featured Teller"
   },
   {
      "teller_last_name" : "Davis",
      "teller_first_name" : "Donald",
      "teller_email" : "donald@ddavisstoryteller.com",
      "teller_id" : 9,
      "teller_phone" : "252-995-2603",
      "teller_image" : "Donald_Davis.jpg",
      "teller_bio" : "Donald Davis was born in a Southern Appalachian mountain world rich in stories, surrounded by a family of traditional storytellers who told him gentle fairy tales, simple and silly Jack tales, scary mountain lore, ancient Welsh and Scottish folktales, and most importantly, nourishing, true-to-life stories of his own neighbors and kin. \n\nFeatured at festivals throughout the U.S. and world, Davis is also known as a prolific author, producer of books and CDs, and as a guest host for NPR's Good Evening.  Davis is a recipient of the NSN ORACLE Circle of Excellence and Lifetime Achievement awards. \n\nI could have listened all morning to Donald Davis' his stories often left listeners limp with laughter at the same time they struggled with a lump in the throat.  -New York Times",
      "teller_website" : "http://www.ddavisstoryteller.com/",
      "teller_role" : "Featured Teller"
   },
   {
      "teller_last_name" : "Thomason",
      "teller_first_name" : "Dovie",
      "teller_email" : "dovie.story@gmail.com",
      "teller_id" : 10,
      "teller_phone" : "301-752-2967",
      "teller_image" : "Dovie_Thomason.jpg",
      "teller_bio" : "Dovie Thomason, a beloved First Nations storyteller, began her journey as a storyteller with the childhood influence of her Kiowa Apache grandmother, Dovie.  Thomason's interpretations of her Kiowa Apache and Lakota ancestors' stories, as well as her original stories, have received international acclaim.\n\nA tradition bearer, writer, teacher and storyteller, she is a multi-talented speaker whose performances are both elegant and witty.  Sometimes wry, occasionally rueful, and always enchanting, Thomason has shared her tales throughout North America and extensively overseas.  She is an NSN ORACLE Circle of Excellence award recipient. \n\nThomason is an outstanding teller with a strong, lyrical, dignified voice.  -School Library Journal",
      "teller_website" : "http://doviethomason.com/",
      "teller_role" : "Featured Teller"
   },
   {
      "teller_last_name" : "Campbell",
      "teller_first_name" : "Kate",
      "teller_email" : "booking@katecampbell.com",
      "teller_id" : 11,
      "teller_phone" : "615-336-6668",
      "teller_image" : "Kate_Campbell.jpg",
      "teller_bio" : "As the daughter of a Mississippi Baptist preacher, Kate Campbell's formative years were spent in the very core of the civil rights movement of the '60s, and the indelible experiences of those years have shaped her heart, character, and convictions. \n\nCampbell's endearing clear-water vocal delivery and easy command of a full range of American music styles have earned her critical acclaim, and her eloquent gift for storytelling has drawn repeated comparison to such bastions of the Southern writing tradition as Flannery O'Connor and Eudora Welty.  Her appearances span the U.S. and abroad, including the Cambridge Folk Festival, and she's been featured on NPR's Morning Edition and All Things Considered. \n\nYou need this woman's music in your life.  -Music Row Magazine",
      "teller_website" : "http://www.katecampbell.com/",
      "teller_role" : "New Voice"
   },
   {
      "teller_last_name" : "Kling",
      "teller_first_name" : "Kevin",
      "teller_email" : "maryeludington@gmail.com",
      "teller_id" : 12,
      "teller_phone" : "",
      "teller_image" : "Kevin_Kling.jpg",
      "teller_bio" : "Best known for his popular commentaries on NPR's All Things Considered and his storytelling stage shows, Kevin Kling delivers hilarious, often tender, stories.  He's from Minneapolis, where he lives on purpose.  Says Kling, When you freeze paradise, it lasts a little longer.  His autobiographical tales are as fascinating as they are true to life: hopping freight trains, getting hit by lightning, and growing up in Minnesota.  \n\nKling has performed all over the U.S. and abroad in Europe, Australia and Thailand, and has been awarded numerous grants and fellowships including from the National Endowment for the Arts.  He is an NSN ORACLE Circle of Excellence award recipient.\n\nPart funny guy, part poet and playwright, part wise man.  \n-Krista Tippett, American Public Media host ",
      "teller_website" : "http://www.kevinkling.com/",
      "teller_role" : "Featured Teller"
   },
   {
      "teller_last_name" : "Gillette",
      "teller_first_name" : "Pipp",
      "teller_email" : "gillettebros@campstreetcafe.com",
      "teller_id" : 17,
      "teller_phone" : "936-636-7165",
      "teller_image" : "Pipp_Gillette.jpg",
      "teller_bio" : "With a lifelong interest in the history of the West and its music, Pipp Gillette plays traditional cowboy music on guitar, banjo, harmonica and bones.  He has performed across the U.S. and abroad, including the Smithsonian Folklife Festival, the Cowboy Poetry Gathering, and tours in England and Japan. \n\nAs the Gillette Brothers, Pipp and his late brother Guy recorded eight albums of traditional cowboy music and were recipients of the Western Heritage Wrangler Award for Outstanding Traditional Album of 2010 as well as the 2012 Wrangler Award for Outstanding Original Western Composition.  Gillette lives in Texas, where he runs a live music venue, the Camp Street Cafe and raises cattle on the family ranch started in 1912. \n\nIt's rare to get to hear such a quality performance with so many layers.  -A.W. Perry Homestead Museum\n",
      "teller_website" : "http://www.campstreetcafe.com/guy&pipp.htm",
      "teller_role" : "New Voice"
   },
   {
      "teller_last_name" : "Lee",
      "teller_first_name" : "Tom",
      "teller_email" : "tellingstories@msn.com",
      "teller_id" : 23,
      "teller_phone" : "860-526-4600",
      "teller_image" : "Tom_Lee.jpg",
      "teller_bio" : "Tom Lee is a professional storyteller with 20 years experience performing traditional stories, folktales and myths for adults and children.  His repertoire is a rich trove of myths and stories from cultures around the world, some that originated thousands of years ago. \n\nLee is the artistic director of artsVOYAGE, a unique arts-in-education program that uses arts to enhance learning across the K-12 curriculum.  Lee, who has shared his work internationally, is a frequent guest artist at the Metropolitan Museum of Art and a performing arts partner with the Yale Center for British Art. \n\nEvery so often you encounter a performer who changes your thinking about familiar art forms.  You step back, reconsider and say 'wow.'  Tom Lee is such a performer.  -Time Out London",
      "teller_website" : "http://www.tomleestoryteller.com/",
      "teller_role" : "New Voice"
   }
]
    EOF
  end

  context "With valid JSON files" do
    context "#each_teller" do
      it "returns new Teller objects" do
        importer.each_teller do |t|
          expect(t).to be_instance_of(Teller)
          expect(t).to be_new_record
        end
      end
    end

    context "#each_event" do
      it "returns new Event objects" do
        importer.each_event do |e|
          expect(e).to be_instance_of(Event)
          expect(e).to be_new_record
        end
      end
    end

    context "#each_event_teller" do
      it "returns a tuple of an Event and a Teller that belongs to it" do
        expect { |b| importer.each_event_teller &b }.to yield_control

        importer.each_event_teller do |event, teller|
          expect(event).to be_instance_of Event
          expect(teller).to be_instance_of Teller
        end
      end
    end

    context "#location_from_id" do
      context "When given a known location id" do
        it "Finds or creates a location in the database" do
          l = importer.location_from_id(3)
          expect(l).to be_instance_of(Location)
          expect(l).not_to be_new_record
          expect(l.name).to eql "Creekside"
        end
      end
    end

    context "#event_time_to_us" do
      context "When the event is on the first day of the festival" do
        it "correctly translates an event start time and day id to the US date format" do
          expect(importer.event_time_to_us("10:00 AM", 5, "2014-10-03")).to eql "10/03/2014 10:00 AM"
        end

        context "And it was classified as special" do
          it "correctly translates an event start time and day id to the US date format" do
            expect(importer.event_time_to_us("2:30 PM", 12, "2014-10-03")).to eql "10/03/2014 2:30 PM"
          end
        end
      end

      context "When the event is on the last day of the festival" do
        it "correctly translates an event start time and day id to the US date format" do
          expect(importer.event_time_to_us("10:00 AM", 7, "2014-10-03")).to eql "10/05/2014 10:00 AM"
        end
      end
    end

    context "#import" do
      it "saves Tellers to the database" do
        count = Teller.count
        _ = importer.import
        new_count = Teller.count

        expect(new_count).to eql(count + importer.teller_count)
      end

      it "saves Events to the database" do
        count = Event.count
        failures = importer.import
        new_count = Event.count

        expect(failures).to be_empty
        expect(new_count).to eql(count + importer.event_count)
      end
    end

    #context "#import" do
    #  it "saves Tellers to the database" do
    #    count = Teller.count
    #    records_imported, _ = csv.import
    #    new_count = Teller.count

    #    expect(new_count).to eql(count + records_imported)
    #  end

    #  it "returns the correct number of imported records" do
    #    records_imported, _ = csv.import
    #    
    #    expect(csv.count).to eql records_imported
    #  end
    #end
  end
end

require_relative '../../../lib/teller_importer'
RSpec.describe TellerImporter::CSV do
  subject(:csv) { TellerImporter::CSV.new(StringIO.new(contents)) }

  let (:contents) do
    <<-EOF
"First Name","Last Name","Website","Role","Bio","Phone"
"Bil","Lepp","http://leppstorytelling.com/","Featured Teller","Bil Lepp is an internationally-known storyteller, author, and recording artist whose style has been described as a satisfying blend of Bill Cosby and Jeff Foxworthy.  A five-time champion of the West Virginia Liars Contest, his outrageous tall-tales and witty stories are a signature of his repertoire. 

Lepp has been featured at venues across the nation, including the Smithsonian Folklife Festival and a recent appearance for Comedy Central.  He is the recipient of the NSN ORACLE Circle of Excellence award, and his picture book, The King of Little Things, won a Parent's Choice Gold Award. 

When [he] wraps up his story in one more burst of implausibility, the audience leaps to its feet, cheering.  -The Dallas Morning News","304-744-2126"
"Bill","Harley","http://www.billharley.com/","Featured Teller","Bill Harley is a two-time Grammy award-winning singer-songwriter, author and monologist.  He is known for his trademark blend of song and story, wit and wisdom, and celebrations of commonality and humanity. 

A longtime commentator for NPR's news program All Things Considered, Harley is the recipient of both a lifetime achievement award from the Rhode Island Council for the Humanities and the NSN ORACLE Circle of Excellence award.  He tours nationwide as an author, performing artist and keynote speaker.  His latest endeavors include a new book series about 4th grader Charlie Bumpers and a family opera entitled Weedpatch. 

Harley delivers an uninhibited performance with wry sense of humor and a love of life we can all share.  -Los Angeles Times","508-336-9703"
"Connie Regan","Blake","http://www.storywindow.com/","Featured Teller","With playful elegance and engaging humor, Connie Regan-Blake takes her listeners on a journey from old-timey mountain tales to surprising, heroic adventures of everyday living.  She is an ambassador for storytelling and a sought-after performer across the globe, having been featured at major folk and storytelling festivals, universities, libraries and conferences in 47 states and 17 countries. 

She is the recipient of the NSN ORACLE Circle of Excellence and Lifetime Achievement awards, and has the unique honor of being the only storyteller to have graced the stage, as performer or emcee, at every National Storytelling Festival. 

  “The mood was mountain and the evening pure magic … Connie’s voice melts to tender tones; then sounds so womanly wise.”  –Southern Living Magazine","828-258-1113"
"Bobby","Norfolk","http://folktale.com/","Featured Teller","Internationally known teaching artist, actor, TV host, recording artist, and professional storyteller since 1975, Bobby Norfolk's unique life path is reflected in his humorous and animated stories.  With tales rich in creativity, lively sound effects, high energy and vibrant 3D characters, his performances promote cultural diversity, self-esteem and character education. 

Norfolk has recorded multiple storytelling CDs which have won ten prestigious Parent's Choice Gold Awards, and has co-authored eight award-winning children's books.  Norfolk has also received three Emmy awards as host of CBS's television series, Gator Tales, and is an NSN ORACLE Circle of Excellence award recipient.

Like an adventure story come to life!  -St. Louis Post Dispatch","314-968-4303"
"Carmen","Deedy","http://carmenagradeedy.com/","Featured Teller","Carmen Deedy is a storyteller and award-winning childrens-book author. Born in Cuba, she moved to the U.S. as a child, and her childhood and family provide a rich vein of material for her vividly told stories. Deedy is aNew York Times best-selling author of ten childrens books and hosts Georgia's three-time Emmy-winning children's program, Love That Book.  

Deedy has been an invited speaker at venues as varied as the Smithsonian Institute, TED, and the Kennedy Center, and is a contributor to NPR.  She is an NSN ORACLE Circle of Excellence award recipient.   

Whenever Carmen tells stories, this Americanized   Cuban Comet leaves a bright trail of laughter and healing.  Kathryn Windham","404-333-2260"
"Carol","Birch","http://www.carolbirchstoryteller.com/","Featured Teller","Carol Birch's style revitalizes language, her art is an absence of artifice, and the stories she tells offer memories worth keeping.  A storyteller and award-winning recording artist, Birch is known for a compelling blend of energy, warmth, vulnerability and directness. In her sure voice, distinguished literature emerges as intimate, conversational and delectable. 

Birch has shared her work around the world in Singapore, Australia, and Europe; her media appearances include NPR and ABC's Nightline.  An NSN ORACLE Circle of Excellence award recipient, Birch teaches at Southern Connecticut State University. 

An articulate, skillful weaver of images and creator of moods...  -Booklist Magazine","203-264-3800"
"Daniel","Morden","https://www.youtube.com/watch?v=REQY5WaV-1g&list=PLfq_6R8RHRsLc2J_H-sBFOkCM8mNXT6VA","International New Voice","Daniel Morden has been a professional storyteller for more than 25 years.  He has traveled across the world, from the Arctic to the Pacific to the Caribbean, to tell and hear traditional stories.  With an engaging manner, Morden humorously and compassionately confronts serious existential questions. 

His appearances include the National Theatre and the British Museum, and he has conceived and presented numerous documentaries on storytelling for BBC Radio Wales. 

To experience Daniel Morden in full flight is an amazing thing.  He combines the skills of the troubadour, the actor, the bard, the standup comedian and the preacher in the pulpit.  One of the UK's greatest storytellers.  -BBC Radio 3.
",""
"Leeny Del","Seamonds","http://www.leenydelseamonds.com/","Featured Teller","With a face and voice that launched a thousand characters, Leeny Del Seamonds is an internationally acclaimed performer, coach, and multi award-winning recording artist.  Her animated and uplifting tales and tunes reflect her love of people and desire to embrace life to its fullest. 

From a prized television show to a village in Gengcun, China to the Comix Club in NYC to the Cayman Islands International Storytelling Festival, Del Seamonds encourages listeners to rejoice in human and cultural diversity and invites them to share in her Cuban-American sense of humor and joy of performing. 

  “Internationally acclaimed Leeny Del Seamonds is a natural entertainer whose multicultural stories and songs reflect her love of people.”  – The Boston Globe","978-692-3961"
"Donald","Davis","http://www.ddavisstoryteller.com/","Featured Teller","Donald Davis was born in a Southern Appalachian mountain world rich in stories, surrounded by a family of traditional storytellers who told him gentle fairy tales, simple and silly Jack tales, scary mountain lore, ancient Welsh and Scottish folktales, and most importantly, nourishing, true-to-life stories of his own neighbors and kin. 

  Featured at festivals throughout the U.S. and world, Davis is also known as a prolific author, producer of books and CDs, and as a guest host for NPR's Good Evening.  Davis is a recipient of the NSN ORACLE Circle of Excellence and Lifetime Achievement awards. 

  I could have listened all morning to Donald Davis' his stories often left listeners limp with laughter at the same time they struggled with a lump in the throat.  -New York Times","252-995-2603"
    EOF
  end

  context "With a valid csv" do
    context "#each_teller" do
      it "returns new Teller objects" do
        csv.each_teller do |t|
          expect(t).to be_instance_of(Teller)
          expect(t).to be_new_record
        end
      end
    end

    context "#import" do
      it "saves Tellers to the database" do
        count = Teller.count
        records_imported, _ = csv.import
        new_count = Teller.count

        expect(new_count).to eql(count + records_imported)
      end

      it "returns the correct number of imported records" do
        records_imported, _ = csv.import
        
        expect(csv.count).to eql records_imported
      end
    end
  end
end

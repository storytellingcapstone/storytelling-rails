require 'rails_helper'

describe Event do
	#let! to force the method’s invocation before each example.
	  let! :event do
	    build :event
	  end

	 #subject : to explicitly define the subject returned in the test cases
	  subject do
	   event
	  end

	  context 'is invalid' do
	    it 'when required #title is not given' do
	      event.title = ''
	      expect(event).not_to be_valid
	    end
	  end
end
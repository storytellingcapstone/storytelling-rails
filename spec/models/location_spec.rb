require 'rails_helper'

describe Location do
	#let! to force the method’s invocation before each example.
	  let! :location do
	    build :location
	  end

	 #subject : to explicitly define the subject returned in the test cases
	 subject do
	   location
	 end

	 context 'is invalid' do
	   it 'when #name is not set' do
	      location.name = ''
	      expect(location).not_to be_valid
	   end
	 end

	context 'When #name is changed to a valid name' do
		it 'is valid' do
			location.name = "College Tent"
			expect(location).to be_valid
		end
	end

  context 'When created with a valid name' do
    it 'is valid' do
      expect(location).to be_valid
    end
  end
end

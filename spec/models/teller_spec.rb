require 'rails_helper'

describe Teller do
	#let! to force the method’s invocation before each example.
	  let! :teller do
	    build :teller
	  end

	 #subject : to explicitly define the subject returned in the test cases
	 subject do
	   teller
	 end

	 context 'is invalid' do
	   it 'when required #first_name is not given' do
	      teller.first_name = ''
	      expect(teller).not_to be_valid
	   end

	    it 'when required #last_name is not given' do
	      teller.last_name = ''
	      expect(teller).not_to be_valid
	   end
	
	 	it "when #phone given is not a valid phone number" do
      		["1112223333", "123222ABCD"].each do |phone_number|
        		teller.phone = phone_number
        		expect(teller).not_to be_valid
      		end
	 	end

	 	it "when #website given is not in valid format" do
      		["http:", "www.abc.com"].each do |website_url|
        		teller.website = website_url
        		expect(teller).not_to be_valid
      		end
	 	end
	end

	context 'is valid' do
		it "when #phone given is a valid phone number" do
      		["4237411150", "850-354-1900"].each do |phone_number|
        		teller.phone = phone_number
        		expect(teller).to be_valid
      		end
	 	end

		it "when #website given is in valid format" do
      		["http://www.abc.com", "http://www.abc.com"].each do |website_url|
        		teller.website = website_url
        		expect(teller).to be_valid
      		end
	 	end

		it "returns a teller's full name as a string" do
			teller = build :teller, first_name: 'John', last_name: 'Doe' # Build the object with the factory, but override the two attributes.
			expect(teller.name).to eq 'John Doe'
		end

    it 'when created by the factory' do
			expect(teller).to be_valid
		end
	end
end

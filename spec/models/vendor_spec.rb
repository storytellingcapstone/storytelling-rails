require 'rails_helper'

describe Vendor do
	#let! to force the method’s invocation before each example.
	  let! :vendor do
	    build :vendor
	  end

	 #subject : to explicitly define the subject returned in the test cases
	 subject do
	   vendor
	 end

	 context 'is invalid' do
	   it 'when required #name is not given' do
	      vendor.name = ''
	      expect(vendor).not_to be_valid
	   end
	 end

	 context 'is valid' do
     it 'when #name changed to a valid name' do
       vendor.name = "Joe's Pizza Parlor"
			expect(vendor).to be_valid
		end

    it 'when created by the factory' do
			expect(vendor).to be_valid
		end
	end
end

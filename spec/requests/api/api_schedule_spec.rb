require 'rails_helper'

RSpec.describe "Schedule API", type: :request do
  before do
    FactoryGirl.create_list(:teller, 10)
    FactoryGirl.create_list(:location, 4)
    FactoryGirl.create(:schedule_with_events)
  end
  describe "GET /api/schedule/sync.json" do
    it "returns successfully when there is a schedule in the database" do
      get "#{api_schedule_sync_path}.json"
      expect(response).to have_http_status(200)
    end
  end
end

#!/usr/bin/env ruby
# Provide support for logging in within feature tests.
# See: https://github.com/plataformatec/devise/wiki/How-To:-Test-with-Capybara

RSpec.configure do |config|
  config.include Warden::Test::Helpers
  config.before :suite do
    Warden.test_mode!
  end
end

module WithAuthenticatedUser
  extend ActiveSupport::Concern

  included do
    before :each do
      admin = create :admin
      login_as(admin, scope: :admin)
    end

    after :each do
      Warden.test_reset!
    end

    def logout
      logout(:admin)
    end
  end
end
